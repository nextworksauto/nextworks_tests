package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestLinksAndDownloadsTabLinks extends LinksAndDownloadsTabLinks{

	@BeforeClass
	public static void main() throws IOException{
		//String capsuleToOpen = "a[href='http://stg.contentcapsule.com/admin/unitcontent/capsules/edit/505']";
		//CMSActivity activity = new CMSActivity();
		//activity.navigateToCapsule(capsuleToOpen);
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
	}
	
	@Test	
	public void test() {
		tabLinks();
		deleteTabLinks();
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
