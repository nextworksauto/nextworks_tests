package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestLinksAndDownloadsTab extends LinksAndDownloadsTab{

	@BeforeClass
	public static void main() throws IOException{
		//String capsuleToOpen = "a[href='http://stg.contentcapsule.com/admin/unitcontent/capsules/edit/551']";
		//CMSActivity activity = new CMSActivity();
		//activity.navigateToCapsule(capsuleToOpen);	
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
	}
	
	@Test
	public void test() {
		links();
		downloads();
		deleteLinksAndDownloads();
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
