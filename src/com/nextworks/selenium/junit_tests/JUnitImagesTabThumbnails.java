package com.nextworks.selenium.junit_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class JUnitImagesTabThumbnails extends ImagesTabThumbnails{

	@BeforeClass
	public static void main() {
		String capsuleToOpen = "a[href='http://stg.contentcapsule.com/admin/unitcontent/capsules/edit/507']";
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(capsuleToOpen);	
		
		ImagesTabActivity tabActivation = new ImagesTabActivity();
		tabActivation.openAndActivateImagesTab();
	}
	
	@Test
	public void test() {
		flickrSection();
		thinglinkSection(); 
		imageUploadSection();
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
