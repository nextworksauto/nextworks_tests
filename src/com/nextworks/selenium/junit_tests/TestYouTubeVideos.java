package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestYouTubeVideos extends YouTubeVideos{

	@BeforeClass
	public static void main() throws IOException{
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
	}	
		
	@Test
	public void test() {
		openAndActivateVideosTab();
		uploadVideos(youTubeArea, addYouTubeButton);
		
		for (int i=0; i<youTubeUrls.length; i++) {
			videosDragAndDrop(youtubeVideosXpathes[i], youTubePanel);
		}
		assertDragAndDrop(cssSelectorForAllEmbedVideos, youTubeUrls);
		
		for (int i=0; i<youTubeUrls.length; i++) {
			deleteAssets(deleteButton);
		}
		
		for (int i=0; i<youTubeUrls.length; i++) {
			String cssSelectorForAssertDeletion = "img[title='"+youTubeUrlsNames[i]+"']";
			assertVideoDeletion(cssSelectorForAssertDeletion);
		}		
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
