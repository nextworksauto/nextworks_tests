package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestImagesTabRollovers extends ImagesTabRollovers{

	@BeforeClass
	public static void main() throws IOException{
		//String capsuleToOpen = "a[href='http://stg.contentcapsule.com/admin/unitcontent/capsules/edit/507']";
		//CMSActivity activity = new CMSActivity();
		//activity.navigateToCapsule(capsuleToOpen);	
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
		
		ImagesTabActivity tabActivation = new ImagesTabActivity();
		tabActivation.openAndActivateImagesTab();
	}
	
	@Test
	public void testFlickrRollovers() {
		flickrSection();
		assertFlickrRollovers();
	}
	
	@Test
	public void testThinglinkRollovers() {
		thinglinkSection();
		assertThinglinkRollovers();
	}
	
	@Test
	public void testImageUploadRollovers() {
		imageUploadSection();
		assertImageUploadRollovers();
	}
	
	@AfterClass
	public static void endOfTest() {
		driver.quit();
	}
	
}
