package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

public class TestImagesTabThinglink extends ImagesTabThinglink{

	@BeforeClass
	public static void main() throws IOException{		
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
	}
	
	@Test
	public void test()  {
		openAndActivateImagesTab();
		expandThinglinkSection();		
		
		for (int i=0; i<thinglinkImages.length; i++) {
			thinglinkUpload(thinglinkImages[i],  thinglinkTitles[i]);
		}
		assertAccetsCreationByCount(allFoundAssets, thinglinkImages);
		
		for (int i=0; i<thinglinkImages.length; i++) {
			dragAndDropImages(thinglinkXpath[i]);
		}
				
		for (int i=0; i>thinglinkImages.length; i++) {
			assertDragAndDropDividedByTwo(allFoundAssets, thinglinkImages);
		}
				
		driver.navigate().refresh();
		waitForDivToLoad();
		driver.findElement(By.cssSelector("a[data-widget='thinglink']")).click();
		
		for (int i=0; i<thinglinkImages.length; i++) {
			deleteAssets(deleteThinglink);
		}
		
		for (int i=0; i<thinglinkImages.length; i++) {
			assertImagesDeletion(thinglinkTitles[i], allFoundAssets);
		}
		
		driver.navigate().refresh();
		assertDeletionByAmount(allFoundAssets);
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
