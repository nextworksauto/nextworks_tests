package com.nextworks.selenium.junit_tests;

import org.junit.BeforeClass;
import org.junit.Test;

public class ImagesTabRollovers extends ImagesTabActivity {

	Thumbnails getThumbnails = new Thumbnails();
	String[] thumbnailsFiles = getThumbnails.thumbnailsFiles;
	
	RolloverImages rollovers = new RolloverImages();
	String[] rolloverImages = rollovers.rolloverImages;
	
	ImagesTabFlickr getNames1 = new ImagesTabFlickr();
	String[] flickrNames = getNames1.flickrTitles;
	
	ImagesTabThinglink getNames2 = new  ImagesTabThinglink();
	String[] thinglinkNames = getNames2.thinglinkTitles;
	
	ImagesTabImageUpload getImagesNames = new ImagesTabImageUpload();
	String[] imagesUploadNames = getImagesNames.imagesNames;
	
	public void flickrSection() {
		ImagesTabThumbnails uploadThumbnails = new ImagesTabThumbnails();
		String[] thumbnailsUploadButton = uploadThumbnails.thumbnailUploadFlickrButtons;
		
		ImagesTabFlickr getImages = new ImagesTabFlickr();
		String[] flickrImages = getImages.flickrUrls;
						
		ImagesTabThumbnails getButton1 = new ImagesTabThumbnails();
		String addFileFromComputer = getButton1.addFileFromComputerForFlickr;
		
		ImagesTabThumbnails getButton2 = new ImagesTabThumbnails();
		String browseButton = getButton2.browseButtonForFlickr;
								
		expandFlickrSection();
		
		for (int i=0; i<flickrImages.length; i++) {
			waitForElementPresent("/html/body/div[5]/div[4]/div[2]/div[6]/form/input[2]"); //wait for "Add photo url" field
			ImagesTabFlickr upload = new ImagesTabFlickr();
			upload.flickrUpload(flickrImages[i], flickrNames[i]);
		}
		
		for (int i=0; i<thumbnailsFiles.length; i++) {
			String browseRolloverImageButton = "/html/body/div[5]/div[4]/div[2]/div[6]/div[2]/form[2]/input[1]";
			RolloverImages rollover = new RolloverImages();
			rollover.addRolloverImageFromTheScratch(thumbnailsUploadButton[i], addFileFromComputer, browseButton, thumbnailsFiles[i], browseRolloverImageButton, rolloverImages[i]);
		}
	}
	
	public void assertFlickrRollovers() {
		for (int i=0; i<thumbnailsFiles.length; i++) {
			RolloverImages assertion = new RolloverImages();
			assertion.assertRolloverImageIsAdded("img[title='"+flickrNames[i]+"']");
		}
	}
	
	public void thinglinkSection() {
		 ImagesTabThumbnails uploadThumbnails = new ImagesTabThumbnails();
		 String[] thumbnailsUploadButton = uploadThumbnails.thumbnailUploadThinglinkButtons;
		
		 ImagesTabThinglink getImages = new  ImagesTabThinglink();
		 String[] thinglinkImages = getImages.thinglinkImages;
		 				 
		 ImagesTabThumbnails getButton1 = new ImagesTabThumbnails();
		 String addFileFromComputer = getButton1.addFileFromYourComputerForThinglink;
			
		 ImagesTabThumbnails getButton2 = new ImagesTabThumbnails();
		 String browseButton = getButton2.browseButtonForThinglink;
		 
		 expandThinglinkSection();
		 
		 for (int i=0; i<thumbnailsFiles.length; i++) { //for thinglink images after each image upload you have to refresh the page prior to thumbnail/rollover upload 
			 String browseRolloverImageButton = "/html/body/div[5]/div[4]/div[2]/div[9]/div[2]/form[2]/input[1]";
			 waitForElementPresent("/html/body/div[5]/div[4]/div[2]/div[9]/form/textarea"); //waiting for textarea to paste thinglink code to be present
			 ImagesTabThinglink upload = new  ImagesTabThinglink();
			 upload.thinglinkUpload(thinglinkImages[i], thinglinkNames[i]);
			 driver.navigate().refresh();
			 waitForDivToLoad();
			 expandThinglinkSection();
			 RolloverImages rollover = new RolloverImages();
			 rollover.addRolloverImageFromTheScratch(thumbnailsUploadButton[i], addFileFromComputer, browseButton, thumbnailsFiles[i], browseRolloverImageButton, rolloverImages[i]);
		 }
	}
	
	public void assertThinglinkRollovers() {
		for (int i=0; i<thumbnailsFiles.length; i++) {
			 RolloverImages assertion = new RolloverImages();
			 assertion.assertRolloverImageIsAdded("img[title='"+thinglinkNames[i]+"']");
		 }
	}
	
	public void imageUploadSection() {
		ImagesTabThumbnails uploadThumbnails = new ImagesTabThumbnails();
		String[] thumbnailsUploadButton = uploadThumbnails.thumbnailUploadImageUploadButtons;
		
		ImagesTabImageUpload getImages = new ImagesTabImageUpload();
		String[] images = getImages.images;
						
		ImagesTabThumbnails getButton1 = new ImagesTabThumbnails();
		String addFileFromComputer = getButton1.addFileFromYourComputerForImageUpload;
		
		ImagesTabThumbnails getButton2 = new ImagesTabThumbnails();
		String browseButton = getButton2.browseButtonForImageUpload;
				
		expandImageUploadSection();
		
		for (int i=0; i<images.length; i++) {
			ImagesTabImageUpload upload = new ImagesTabImageUpload();
			upload.uploadImage(images[i], imagesUploadNames[i]);
		}
		
		for (int i=0; i<thumbnailsFiles.length; i++) {
			String browseRolloverImageButton = "/html/body/div[5]/div[4]/div[2]/div[11]/div[3]/form[2]/input[1]";
			RolloverImages rollovers = new RolloverImages();
			rollovers.addRolloverImageFromTheScratch(thumbnailsUploadButton[i], addFileFromComputer, browseButton, thumbnailsFiles[i], browseRolloverImageButton, rolloverImages[i]);
		}				
	}
	
	public void assertImageUploadRollovers() {
		for (int i=0; i<thumbnailsFiles.length; i++) {
			 RolloverImages assertion = new RolloverImages();
			 assertion.assertRolloverImageIsAdded("img[title='"+imagesUploadNames[i]+"']");
		 }	
	}
	
	
}
