package com.nextworks.selenium.junit_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CMSActivity {
	
	protected static final String SERVER_NAME = "stg.contentcapsule.com/admin";
	protected static final String USER_NAME = "ksafford@nextworks.com";
	protected static final String PROTOCOL = "http";
	protected static final String PASSWORD = "1234567k";
	protected static WebDriver driver;
	
	protected void navigateToCapsule(String capsuleUrl) {
		setWebDriver();
		driver.get(getContentURL());
		driver.manage().window().maximize();		
		loginPage();
		openCapsule(capsuleUrl);
		waitForDivToLoad();
	}
	
	protected static String getCapsuleUrl() throws IOException {
		String pathToFile ="/Users/ksafford/NextworksTests/NewCapsuleUrl.txt";
		BufferedReader reader = new BufferedReader (new FileReader(pathToFile));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		//String ls = System.getProperty("line.separator");
		
		while( (line = reader.readLine()) !=null) {
			stringBuilder.append(line);
			//stringBuilder.append(ls);
		}
		return "a[href='"+stringBuilder.toString()+"']";
				
	}
	
	protected void setWebDriver() {
		driver = new FirefoxDriver();
	}
	
	protected String getContentURL() {
		return PROTOCOL + "://" + SERVER_NAME + "/content";
	}
	
	protected void loginPage() {
		WebElement username = driver.findElement(By.id("login_value")); 
		username.sendKeys(USER_NAME);
		WebElement password = driver.findElement(By.id("password"));
		password.sendKeys(PASSWORD);
		WebElement letMEIn = driver.findElement(By.id("submit"));
		letMEIn.click();
	}
	
	protected void openCapsule(String capsuleLink) {
		WebElement editCapsuleContent = driver.findElement(By.cssSelector(capsuleLink));
		editCapsuleContent.click();
		waitForDivToLoad();
	}
		
	protected void exit() {
		driver.quit();
	}
	
	protected void enterField(String fieldName, String value) {
		driver.findElement(By.id(fieldName)).clear();
		driver.findElement(By.id(fieldName)).sendKeys(value);
	}
	
	protected void dropdownList (String fieldName, String value) {
		Select dropdownList = new Select(driver.findElement(By.id(fieldName)));
		dropdownList.selectByVisibleText(value);
	}
	
	protected void waitForDivToLoad() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("working_overlay")));
	}
			
	protected void openAndActivateTab(String checkBoxOnTab, String clickOnTab) {
		WebElement linksDownloadsTabCheckBox = driver.findElement(By.name(checkBoxOnTab));
		if (!linksDownloadsTabCheckBox.isSelected()) {
			linksDownloadsTabCheckBox.click();			
		}		
		driver.findElement(By.cssSelector(clickOnTab)).click();
	}
	
	protected void waitForElementPresent(String xpath) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}
		
	public void assertAssetsCreation(String xpath, String givenTitle) {
		WebElement tabLink = driver.findElement(By.xpath(xpath));
		String tabLinkTitle = tabLink.getAttribute("title");
		assertEquals(tabLinkTitle, givenTitle);
	}
	
	public void assertDragAndDrop(String cssSelector, String[] arrayWithAssets) {
		List <WebElement> embedAssets = driver.findElements(By.cssSelector(cssSelector));
		int amountOfEmbedAssets = embedAssets.size();
		//System.out.println("The amount of embed assets is " + amountOfEmbedAssets);
		assertEquals(amountOfEmbedAssets, arrayWithAssets.length);		
	}
	
	public void assertDragAndDropDividedByTwo(String cssSelector, String[] array) {
		List <WebElement> embedAssets = driver.findElements(By.cssSelector(cssSelector));
		int amountOfAssets = embedAssets.size();
		//System.out.println("The amount of elements is " + amountOfAssets);
		assertEquals(amountOfAssets/2, array.length); // we divide by two because the name of uploaded pictures in right-hand panel and in carousel are same///// So if you have 3 embed pictures - the findElements will find 6 of them instead - 3 in carousel+3 in right-hand panel
	}
	
	public void deleteAssets(String xpath) {
		driver.findElement(By.xpath(xpath)).click();
		waitForDivToLoad();
	}
	
	public void assertDeletionByAmount(String cssSelector) {
		List <WebElement> allVideos = driver.findElements(By.cssSelector(cssSelector));
		//System.out.println("The amount of embed images is"+allVideos.size());
		assertEquals(allVideos.size(), 0);
	}
	
	public void changeTabTitle(String titleFieldId, String newTabTitle, String saveButton) {
		driver.findElement(By.id(titleFieldId)).clear();
		driver.findElement(By.id(titleFieldId)).sendKeys(newTabTitle);
		driver.findElement(By.id(saveButton)).click();
		waitForDivToLoad();
	}
	
	public void previewCapsule() {
		WebElement previewCapsule = driver.findElement(By.xpath("/html/body/div[3]/div/div/ul/li[2]/a"));
		previewCapsule.click();
	}
	
	public void assertChangeInPreview(String givenTabName) {
		//News Media tab has to be the only activated tab and has to have no image tab for this method to work
		String window1 = driver.getWindowHandle();
				
		previewCapsule();
		
		waitForElementPresent("/html/body/div/div[4]/div[2]");	//wait for page to load			
		assertTrue(driver.getPageSource().contains(givenTabName));
		
		driver.switchTo().window(window1);				
	}
	
	public void assertTextFieldChange(String tabTitleFieldId, String newTitle) {
		String givenTitle = driver.findElement(By.id(tabTitleFieldId)).getAttribute("value");
		assertEquals(givenTitle, newTitle);
	}
	
	public void addTabImage(String addTabImageButton, String pathToImage) {
		driver.findElement(By.id(addTabImageButton)).sendKeys(pathToImage);
		waitForDivToLoad();
	}
	
	public void assertTabImageAddInEditor(String tabImageId) {
		WebElement tabImage = driver.findElement(By.id(tabImageId));
		assertTrue(tabImage.isDisplayed());
	}
	
	public void assertTabImageAddInPreview() {
		previewCapsule();		
		WebElement tabImageInPreview = driver.findElement(By.xpath("/html/body/div/div[4]/div[2]/h1/img"));
		assertTrue(tabImageInPreview.isDisplayed());		
	}
	
	public void deleteTabImage(String removeImageButon) {
		driver.findElement(By.id(removeImageButon)).click();
		waitForDivToLoad();
	}
	
	public void assertTabImageDeletion(String removeTabImageButtonId) {
		assertFalse(driver.findElement(By.id(removeTabImageButtonId)).isDisplayed());
	}
	
	public void assertTextIsEntered(String textToFind) {
		driver.getPageSource().contains(textToFind);
	}
	
}


