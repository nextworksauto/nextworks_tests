package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestImagesTabImageUpload extends ImagesTabImageUpload {

	@BeforeClass
	public static void main() throws IOException{
		//String capsuleToOpen = "a[href='http://stg.contentcapsule.com/admin/unitcontent/capsules/edit/551']";
		//CMSActivity activity = new CMSActivity();
		//activity.navigateToCapsule(capsuleToOpen);
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
	}
	
	@Test
	public void test() {
		openAndActivateImagesTab();		
		expandImageUploadSection();

		for (int i=0; i<images.length; i++) {
			uploadImage(images[i], imagesNames[i]);
		}
				
		//driver.findElement(By.xpath("/html/body/div[5]/div[4]/div[2]/div[10]/a")).click();
		//for (int i=0; i<images.length; i++) {
		//	assertAssetsCreation(imagesXpathes[i], imagesNames[i]);
		//}
				
		driver.navigate().refresh();
		waitForDivToLoad();
		expandImageUploadSection();
		
		for (int i=0; i<images.length; i++) {
			dragAndDropUploadedImages(imagesXpathes[i]);
		}
		
		for (int i=0; i<images.length; i++) {
			assertDragAndDropDividedByTwo(allFoundAssets, images);
		}
		
		//delete from right-hand panel
		for (int i=0; i<images.length; i++) {
			deleteAssets(deleteXpathes[deleteXpathes.length-i-1]);
		}
		
		for (int i=0; i<images.length; i++) {
			assertImagesDeletion(imagesNames[i], xpathToAssertDeletiionFromRightHandPanel);
		}
				
		//delete from carousel
		for (int i=0; i<images.length; i++) {
			deleteAssets(deleteFromCarousel);
		}
		assertDeletionByAmount(allFoundAssets);
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
