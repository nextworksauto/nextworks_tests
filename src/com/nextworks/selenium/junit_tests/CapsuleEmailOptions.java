package com.nextworks.selenium.junit_tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CapsuleEmailOptions extends CMSActivity{

	private static String emailSubject = "Test subject";
	private static String generateEmailBody(WebDriver driver) { 
		return "The URL of this capsule is" + driver.getCurrentUrl();
	}		
	
	public void emailPanel() {
		driver.findElement(By.id("email_panel_close")).click();		
		
		enterField("email_canvas_colour", emailSubject);
		enterField("email_body", generateEmailBody(driver));
		
		String saveEmailPanelButton = ".//*[@id='submit'][@value='Save email settings']";
		driver.findElement(By.xpath(saveEmailPanelButton)).click();
		waitForDivToLoad();
	}
	
	public void checkEmailPanel() throws IOException{
		driver.get(getContentURL());
		openCapsule(getCapsuleUrl());
		waitForDivToLoad();
		
		driver.findElement(By.id("email_panel_close")).click();
		
		String inputValue = driver.findElement(By.id("email_canvas_colour")).getAttribute("value");
		assertEquals("Test subject", inputValue);
				
		String inputValue2 = driver.findElement(By.id("email_body")).getAttribute("value");
		assertEquals(generateEmailBody(driver), inputValue2);
	}
}
