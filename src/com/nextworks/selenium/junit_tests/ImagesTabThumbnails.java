package com.nextworks.selenium.junit_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ImagesTabThumbnails extends ImagesTabActivity{

	Thumbnails getThumbnails = new Thumbnails();
	String[] thumbnailsFiles = getThumbnails.thumbnailsFiles;
	
	String[] thumbnailUploadFlickrButtons = {
			"/html/body/div[5]/div[4]/div[2]/div[6]/div[1]/div/div[2]/a[1]/i",
			"/html/body/div[5]/div[4]/div[2]/div[6]/div[1]/div/div[3]/a[1]/i",
			"/html/body/div[5]/div[4]/div[2]/div[6]/div[1]/div/div[4]/a[1]/i"
		};
	
	String[] thumbnailUploadImageUploadButtons = {
			"/html/body/div[5]/div[4]/div[2]/div[11]/div[1]/div/div[2]/div[1]/a[1]/i",
			"/html/body/div[5]/div[4]/div[2]/div[11]/div[1]/div/div[2]/div[2]/a[1]/i",
			"/html/body/div[5]/div[4]/div[2]/div[11]/div[1]/div/div[3]/div/a[1]/i"
	};
	
	String[] thumbnailUploadThinglinkButtons = {
			"/html/body/div[5]/div[4]/div[2]/div[9]/div[1]/div[2]/a[1]/i",
			"/html/body/div[5]/div[4]/div[2]/div[9]/div[1]/div[3]/a[1]/i",
			"/html/body/div[5]/div[4]/div[2]/div[9]/div[1]/div[4]/a[1]/i"
	};
	
	String addFileFromComputerForFlickr = "/html/body/div[5]/div[4]/div[2]/div[6]/div[2]/div/input[2]";
	String browseButtonForFlickr = "/html/body/div[5]/div[4]/div[2]/div[6]/div[2]/form[1]/input[1]";
	
	String addFileFromYourComputerForThinglink = "/html/body/div[5]/div[4]/div[2]/div[9]/div[2]/div/input[2]"; 
	String browseButtonForThinglink = "/html/body/div[5]/div[4]/div[2]/div[9]/div[2]/form[1]/input[1]";
	
	String addFileFromYourComputerForImageUpload = "/html/body/div[5]/div[4]/div[2]/div[11]/div[3]/div/input[2]";
	String browseButtonForImageUpload = "/html/body/div[5]/div[4]/div[2]/div[11]/div[3]/form[1]/input[1]";
		
	public void flickrSection() {
		ImagesTabFlickr getImages = new ImagesTabFlickr();
		String[] flickrImages = getImages.flickrUrls;
		
		ImagesTabFlickr getNames = new ImagesTabFlickr();
		String[] flickrNames = getNames.flickrTitles;
				
		expandFlickrSection();
		
		for (int i=0; i<flickrImages.length; i++) {
			ImagesTabFlickr upload = new ImagesTabFlickr();
			upload.flickrUpload(flickrImages[i], flickrNames[i]);
		}
				
		for (int i=0; i<thumbnailsFiles.length; i++) {
			String closeRolloverDialog = "/html/body/div[5]/div[4]/div[2]/div[6]/div[2]/a";
			Thumbnails thumbnails = new Thumbnails();
			thumbnails.addThumbnails(thumbnailUploadFlickrButtons[i], addFileFromComputerForFlickr, browseButtonForFlickr, thumbnailsFiles[i], closeRolloverDialog);
		}		
	}
	
	public void thinglinkSection() { //for thinglink images after each image upload you have to refresh the page prior to thumbnail/rollover upload
		ImagesTabThinglink getImages = new ImagesTabThinglink();
		String[] thinglinkImages = getImages.thinglinkImages;
		
		ImagesTabThinglink getImagesNames = new ImagesTabThinglink();
		String[] thinglinkTitles = getImagesNames.thinglinkTitles;
				
		expandThinglinkSection();
		
		for (int i=0; i<thinglinkImages.length; i++) {
			String closeRolloverDialog = "/html/body/div[5]/div[4]/div[2]/div[9]/div[2]/a";
			waitForElementPresent("/html/body/div[5]/div[4]/div[2]/div[9]/form/textarea"); //waiting for textarea to paste thinglink code to be present
			ImagesTabThinglink upload = new ImagesTabThinglink();
			upload.thinglinkUpload(thinglinkImages[i], thinglinkTitles[i]);
			driver.navigate().refresh();
			waitForDivToLoad();
			expandThinglinkSection();
			Thumbnails action = new Thumbnails();
			action.addThumbnails(thumbnailUploadThinglinkButtons[i], addFileFromYourComputerForThinglink, browseButtonForThinglink, thumbnailsFiles[i], closeRolloverDialog);
		}				
	}
		
	public void imageUploadSection() {
		ImagesTabImageUpload getImages = new ImagesTabImageUpload();
		String[] images = getImages.images;
		
		ImagesTabImageUpload getImagesNames = new ImagesTabImageUpload();
		String[] imagesNames = getImagesNames.imagesNames;
				
		expandImageUploadSection();
		
		for (int i=0; i<images.length; i++) {
			ImagesTabImageUpload upload = new ImagesTabImageUpload();
			upload.uploadImage(images[i], imagesNames[i]);
		}
		
		for (int i=0; i<thumbnailsFiles.length; i++) {
			String closeRolloverDialog = "html/body/div[5]/div[4]/div[2]/div[11]/div[3]/a";
			Thumbnails thumbnails = new Thumbnails();
			thumbnails.addThumbnails(thumbnailUploadImageUploadButtons[i], addFileFromYourComputerForImageUpload, browseButtonForImageUpload, thumbnailsFiles[i], closeRolloverDialog);
		}
	}
	
	
	
}
