package com.nextworks.selenium.junit_tests;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class YouTubeVideos extends VideosTabActivity{
	String[] youTubeUrls = {
											"https://www.youtube.com/watch?v=BCxCRI2Qv6U",
											"https://www.youtube.com/watch?v=lyKrCAgki-Y",
											"https://www.youtube.com/watch?v=IEVE3KSKQ0o"			
											};
	
	String[] youTubeUrlsNames = {
												"Love and Death [Woody Allen] - To Suffer [PL]",
												"Annie Hall (1977) Best Scenes",
												"Ant-Man Official Teaser Trailer #1 (2015) - Paul Rudd Marvel Movie HD"
												};
	
	String[] youtubeVideosXpathes = {
			  ".//*[@id='vid_list']/div[1]/img",
			  ".//*[@id='vid_list']/div[2]/img",
			  ".//*[@id='vid_list']/div[3]/img"
			  };
	
	String youTubeArea = "youtube_video_videoId";
	
	String addYouTubeButton = "/html/body/div[5]/div[4]/div[2]/div[2]/form/input";
	
	String youTubePanel = "/html/body/div[5]/div[4]/div[2]/div[1]/a";
	
	String cssSelectorForAllEmbedVideos = "img[data-content_item_type='youtube_video']";
	
	String deleteButton = "/html/body/div[5]/div[4]/div[2]/div[2]/div[1]/div/div/a[2]/i";
	
	public void uploadVideos(String youTubeAreaId, String addButton) {
		WebElement youTubeUrltextArea = driver.findElement(By.id(youTubeAreaId));
				if (!youTubeUrltextArea.isDisplayed()) {
			//expand YouTube section
			driver.findElement(By.xpath("/html/body/div[5]/div[4]/div[2]/div[1]/a")).click();
		}
		
		for (int i=0; i<youTubeUrls.length; i++) {
			youTubeUrltextArea.clear();
			youTubeUrltextArea.sendKeys(youTubeUrls[i]);
			driver.findElement(By.xpath(addButton)).click();
			waitForDivToLoad();
		}
		
		for (int i=0; i<youTubeUrls.length; i++) {
			assertAssetsCreation(youtubeVideosXpathes[i], youTubeUrlsNames[i]);
		}
	}
			
	public void assertVideoDeletion(String cssSelector) {
		List <WebElement> allVideos = driver.findElements(By.cssSelector(cssSelector));
		
		ArrayList<String> allTabLinksInArray = new ArrayList<String>();
		for (int i=0; i<allVideos.size()-1; i++) {
			allTabLinksInArray.add(allVideos.get(i).getAttribute("title"));
		}
		
		for (int i=0; i<allVideos.size()-1; i++) {
			assertFalse(allTabLinksInArray.contains(youTubeUrlsNames[i]));
		}
	}
	
	/*public void assertVideoDeletion(String cssSelector) {
		List <WebElement> allVideos = driver.findElements(By.cssSelector(cssSelector));
		System.out.println("The amount of Tab Links in the system is "+allVideos.size());
						
		List<Integer> allVideosAmount = new ArrayList<Integer>();
		allVideosAmount.add(allVideos.size());
				
		System.out.println(allVideosAmount);
				
		for (int i=0; i<youTubeUrls.length; i++) {
			assertEquals(allVideos.size(), youTubeUrls.length-i-1);
		}
	}
	*/
	

}
