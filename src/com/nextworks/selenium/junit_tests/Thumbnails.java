package com.nextworks.selenium.junit_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Thumbnails extends CMSActivity {

	String[] thumbnailsFiles = {
			"/Users/ksafford/Desktop/Documents/test images/ball.png",
			"/Users/ksafford/Desktop/Documents/test images/birdie.jpg",
			"/Users/ksafford/Desktop/Documents/test images/fishOne.jpg"
		};
	
	
	public void addThumbnails(String addThumbnailButton, String addFileFromYourComputerRadioButton, String browseButton, String thumbnailFile, String closeRolloverImageDialog) {
		WebElement thumbnailButton = driver.findElement(By.xpath(addThumbnailButton));
		thumbnailButton.click();
		
		WebElement addThumbnailFromComputer = driver.findElement(By.xpath(addFileFromYourComputerRadioButton));
		addThumbnailFromComputer.click();
		
		WebElement uploadThumbnail = driver.findElement(By.xpath(browseButton));
		uploadThumbnail.sendKeys(thumbnailFile);
		waitForDivToLoad();
		
		WebElement closeRolloverImage = driver.findElement(By.xpath(closeRolloverImageDialog));
		closeRolloverImage	.click();
	}
	
	
	
	
}
