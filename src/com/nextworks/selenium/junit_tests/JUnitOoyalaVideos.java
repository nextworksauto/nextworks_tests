package com.nextworks.selenium.junit_tests;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class JUnitOoyalaVideos extends VideosTabActivity {

	String[] ooyalaUrls = {
			"<script src='//player.ooyala.com/v3/eaa976796c1b44558231024eb94144c0'></script><div id='ooyalaplayer' style='width:640px;height:360px'></div><script>OO.ready\\(function\\(\\) { OO.Player.create\\('ooyalaplayer', 'ppdjA2czpA9Usj2ngcVga8RClzRdt51L', {\"autoplay\":true}\\); }\\);</script><noscript><div>Please enable Javascript to watch this video</div></noscript>",
		  //"<script src='//player.ooyala.com/v3/eaa976796c1b44558231024eb94144c0'></script><div id='ooyalaplayer' style='width:640px;height:360px'></div><script>OO.readyfunction) { OO.Player.create'ooyalaplayer', 'pndjA2czr9wrsy6Ma79wfe_09JzvReFS', {\"autoplay\":true}); });</script><noscript><div>Please enable Javascript to watch this video</div></noscript>",
			"<script src='//player.ooyala.com/v3/eaa976796c1b44558231024eb94144c0'></script><div id='ooyalaplayer' style='width:640px;height:360px'></div><script>OO.ready(function() { OO.Player.create('ooyalaplayer', 'pndjA2czr9wrsy6Ma79wfe_09JzvReFS', {\"autoplay\":true}); });</script><noscript><div>Please enable Javascript to watch this video</div></noscript>",
			"<script src='//player.ooyala.com/v3/eaa976796c1b44558231024eb94144c0'></script><div id='ooyalaplayer' style='width:1280px;height:720px'></div><script>OO.ready(function() { OO.Player.create('ooyalaplayer', 'prdjA2czqzwhSWurK3qvUztR2QRjOL7_', {\"autoplay\":true}); });</script><noscript><div>Please enable Javascript to watch this video</div></noscript>",
	};
	
	String[] ooyalaXpathes = {
		"/html/body/div[5]/div[4]/div[2]/div[13]/div[1]/div/div[1]/img",
		"/html/body/div[5]/div[4]/div[2]/div[13]/div[1]/div/div[2]/img",
		"/html/body/div[5]/div[4]/div[2]/div[13]/div[1]/div/div[3]/img"
	};
	
	String ooyalaPanel = "/html/body/div[5]/div[4]/div[2]/div[12]/a";
						  
	@BeforeClass
	public static void main(){
		String capsuleToOpen = "a[href='http://stg.contentcapsule.com/admin/unitcontent/capsules/edit/460']";
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(capsuleToOpen);	
	}	
	
	@Test
	public void test() {
		String ooyalaEmbedCode = "ooyala_embed_code";
		String addOoyalaButton = "/html/body/div[5]/div[4]/div[2]/div[13]/form/input[1]";
		
		openAndActivateVideosTab();		
				
		driver.findElement(By.xpath(ooyalaPanel)).click();
		
		driver.findElement(By.id(ooyalaEmbedCode)).sendKeys(ooyalaUrls[2]);
		driver.findElement(By.id(ooyalaEmbedCode)).sendKeys(Keys.ENTER);
		
		//uploadVideos(ooyalaEmbedCode, addOoyalaButton);
		
				
		//for (int i=0; i<ooyalaUrls.length; i++) {
		//	videosDragAndDrop(ooyalaXpathes[i], ooyalaPanel);
		//}		
		
	}
	
	public void uploadVideos(String youTubeAreaId, String addButton) {
		WebElement youTubeUrltextArea = driver.findElement(By.id(youTubeAreaId));
				if (!youTubeUrltextArea.isDisplayed()) {
			//expand Ooyala section
			driver.findElement(By.xpath(ooyalaPanel)).click();
		}
		
		for (int i=0; i<ooyalaUrls.length; i++) {
			youTubeUrltextArea.clear();
			youTubeUrltextArea.sendKeys(ooyalaUrls[i]);
			driver.findElement(By.xpath(addButton)).click();
			waitForDivToLoad();
		}
				
	}
	
}
