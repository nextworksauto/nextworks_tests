package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TestPresentationsTab extends PresentationsTab{

	@BeforeClass
	public static void main() throws IOException{		
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
	}
	
	@Test
	public void test() {
		openAndActivateTab(presentationsTabCheckBox, presentationsTab);
		
		WebElement expandPresentationsRightHandPanel = driver.findElement(By.xpath("/html/body/div[5]/div[4]/div[2]/div[3]/a"));
		expandPresentationsRightHandPanel.click();
		
		for (int i=0; i<presentations.length; i++) {
			addPresentation(presentations[i], presentationTitles[i]);
		}
		
		for (int i=0; i<presentations.length; i++) {
			 assertPresentationCreation(presentationsXpath[i], presentationTitles[i]);
		}
		
		 for (int i=0; i<presentations.length; i++) {
			 presentationsDragAndDrop(presentationsXpath[i], "/html/body/div[5]/div[4]/div[2]/div[3]/a");
		 }
		
		 for (int i=0; i<presentations.length; i++) {
			 assertDragAndDropDividedByTwo(cssSelectorForAllEmbedPresentations, presentations);
		 } 
		
		 for (int i=0; i<presentations.length; i++) {
			 deleteAssets(deleteButton);
		 }		 
		 assertDeletionByAmount(cssSelectorForAllEmbedPresentations);		
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
