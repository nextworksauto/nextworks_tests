package com.nextworks.selenium.junit_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class ImagesTabFlickr extends ImagesTabActivity {
	
	String[] flickrUrls = {
			"https://www.flickr.com/photos/1-6-scale-doll-clothes/6898005348/in/photolist-bvy5EG-7Ph91h-ajDtdB-6xHfyh-arrJfr-nRQ8kD-c9j74u-fpRtUs-dJT4Pr-9BNWjV-iZRWtg-pVY4Qi-kdMcca-bYptZ1-9GDc9U-63NuSa-62wMd9-nRQ7xg-4zSecz-d2BdXJ-driA28-jofh42-67bchR-my8xKk-3wfCMB-mLad7X-a5wE6k-eYG1mD-dQaT5q-aDXWwa-cHxvXm-d5h4fU-prCh6P-bY3jqw-dy8MBP-c4FNd9-ooe2cQ-4LySrk-nzFL3p-6WfYnJ-oqAXAY-okai5R-ddfBDC-nW1aKu-dzN52u-bXn17u-mfEa21-but9Yt-nKGnuB-hkNdLG",
			"https://www.flickr.com/photos/yillup/6118218543/in/photolist-ajDtdB-6xHfyh-arrJfr-nRQ8kD-c9j74u-fpRtUs-dJT4Pr-9BNWjV-iZRWtg-pVY4Qi-kdMcca-bYptZ1-9GDc9U-63NuSa-62wMd9-nRQ7xg-4zSecz-d2BdXJ-driA28-jofh42-67bchR-my8xKk-3wfCMB-mLad7X-a5wE6k-eYG1mD-dQaT5q-aDXWwa-cHxvXm-d5h4fU-prCh6P-bY3jqw-dy8MBP-c4FNd9-ooe2cQ-4LySrk-nzFL3p-6WfYnJ-oqAXAY-okai5R-ddfBDC-nW1aKu-dzN52u-bXn17u-mfEa21-but9Yt-nKGnuB-hkNdLG-bfHmyn-8Vgdrx",
			"https://www.flickr.com/photos/raffacama/4717398947/in/photolist-7CAgir-b8ELX4-89TwTZ-75gYXb-7zyxju-8bRUmB-9GvAzZ-9RMrHr-79rNqa-71y8nE-9JCaB9-cZoYaj-aPEyGp-bDoSAT-aBbZwJ-bnGuJ5-9Ltwwm-g5bPA3-nQ8j6f-nz1gu8-79Jt6G-dmryuS-9PBRQC-nYrkdj-8bHA6J-oCYWA4-pTiwLm-8bHScy-eXxNGN-iU1f8W-hA21oB-iAoDM4-e86e8X-dRNyXn-edNb4R-e8scUW-dnLUCG-dTbbw6-e8Lxy6-fziaTM-dUGHYF-9ZL7yW-ecm4Ph-furW61-fvdDMi-fp3ikf-fEDd7P-e8sbeW-ege9Dw-e8Ab3X"
		     };
	
	String[] flickrTitles = {
			 "flickr_image_1",
			 "flickr_image_2",
			 "flickr_image_3"};
	
	String[] flickrXpath = {
			".//*[@id='flick_list']/div[2]/img",
			".//*[@id='flick_list']/div[3]/img",
			".//*[@id='flick_list']/div[4]/img"
	};
	
	String allFoundAssets = "img[data-content_item_type='flickr']";
	
	public void flickrUpload(String flickrImageURL, String flickrImageTitle) {
		WebElement flickrUrl = driver.findElement(By.id("flickr_photo_id"));
		flickrUrl.sendKeys(flickrImageURL);
		WebElement flickrTitle = driver.findElement(By.id("photo_title"));
		flickrTitle.sendKeys(flickrImageTitle);
		flickrTitle.sendKeys(Keys.ENTER);
		waitForDivToLoad();
	}
	
	
} 
