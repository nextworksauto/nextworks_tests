package com.nextworks.selenium.junit_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class LinksAndDownloadsTabLinks extends LinksAndDownloadsActivity{

	public String[] tabLinks = {
			"Images Tab",
			"Videos Tab",
			"Press Release Tab"
	};
		
	public String[] tabLinksNames = {
			"Go to Images Tab",
			"Go to Videos Tab",
			"Go to Press Release Tab"
	};
		
	public String[] tabLinksXpathes = {
			"/html/body/div[5]/div[4]/div[2]/div[21]/div[1]/div/div[2]/img",
			"/html/body/div[5]/div[4]/div[2]/div[21]/div[1]/div/div[3]/img",
			"/html/body/div[5]/div[4]/div[2]/div[21]/div[1]/div/div[4]/img"
	};
	
	public void tabLinks() {
		openAndActivateLinkAndDownloadsTab();
		//expand Links/Downloads Tab Links section
		driver.findElement(By.cssSelector("a[data-widget='tab_links']")).click();		
		
		for(int i=0; i<tabLinks.length; i++) {
			addTabLinks(tabLinks[i], tabLinksNames[i]);
		}
				
		for(int i=0; i<tabLinks.length; i++) {
			assertAssetsCreation(tabLinksXpathes[i], tabLinksNames[i]);
		}				
	}
	
	public void deleteTabLinks() {
		String cssSelectorForAssertDeletion = "div[data-content_item_type='tab_link']";
				
		String[] tabLinksToDelete = {
				"/html/body/div[5]/div[4]/div[2]/div[21]/div[1]/div/div[2]/a[2]/i",
				"/html/body/div[5]/div[4]/div[2]/div[21]/div[1]/div/div[3]/a[2]/i",
				"/html/body/div[5]/div[4]/div[2]/div[21]/div[1]/div/div[4]/a[2]/i"
		};
		
		LinksAndDownloadsTabLinks arrayNames = new LinksAndDownloadsTabLinks();
		String[] tabLinksNames = arrayNames.tabLinksNames;
				
		for (int i=0; i<tabLinksNames.length; i++){
			String expandPanel = "a[data-widget='tab_links']";
			deleteLinksAndDownloads(tabLinksToDelete[tabLinksToDelete.length-i-1], expandPanel);
			assertTabLinkDeletion(tabLinksNames[tabLinksNames.length-i-1], cssSelectorForAssertDeletion);
		}
	}
	
	public void addTabLinks (String tabLinkName, String tabLinkTitle) {
		 WebElement selectTabList = driver.findElement(By.id("tab_id"));
		 if (!selectTabList.isDisplayed()) {
			 driver.navigate().refresh();
			 waitForDivToLoad();
		 }
		 dropdownList("tab_id", tabLinkName);
		 
		 WebElement nameOfTabLink = driver.findElement(By.xpath("/html/body/div[5]/div[4]/div[2]/div[21]/div[2]/form/input[1]")); 
		 if (!nameOfTabLink.isDisplayed()){
			 String expandTabLinksSection = "/html/body/div[5]/div[4]/div[2]/div[20]/a";
			 driver.findElement(By.xpath(expandTabLinksSection)).click();
		 }
		 nameOfTabLink.clear();
		 nameOfTabLink.sendKeys(tabLinkTitle);
		 nameOfTabLink.sendKeys(Keys.ENTER);
		 waitForDivToLoad();
	}
			
	public String[] getTabLinksNames() {
		String[] tabLinksNames = {
				"Images Tab",
				"Videos Tab",
				"Press Release Tab"
		};
		return tabLinksNames;
	}

	public String[] getTabLinksXpathes() {
		return tabLinksXpathes;
	}
}


