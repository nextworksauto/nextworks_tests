package com.nextworks.selenium.junit_tests;

import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class NewsMediaTab extends CMSActivity {

	String newTitle = "Contact Info";
	
	String mediaLinkFieldId = "news_media_link";
	String mediaLinkTextFieldId = "news_media_link_text";
	String mediaLinkUrl = "http://www.google.com";
	String mediaLinkText = "open google";
	
	String mainMessage = "News Media Test Message";
	
	String tabImageFile = "/Users/ksafford/Desktop/Documents/test images/plant2.jpg";
	
	public void changeNewsMediaTitle() {
		String changeTitleFieldId = "news_media_tab_title";
		String saveTitleButton = "save_news_media_tab_text";
		changeTabTitle(changeTitleFieldId, newTitle, saveTitleButton);
		
		String tabNameFieldId = "news_media_tab_title";
		assertTextFieldChange(tabNameFieldId, newTitle);
				
		assertChangeInPreview(newTitle);
	}
	
	public void changeTabImage() {
		String addTabImageButton = "news_media_tab_image";
		addTabImage(addTabImageButton, tabImageFile);
		
		String tabImageId = "current_news_media_tab_image";
		assertTabImageAddInEditor(tabImageId);
		
		//add AssertImageAdd in preview mode method here
		
		String deleteTabImageButton = "remove_news_media_tab_image";
		deleteTabImage(deleteTabImageButton);
		
		String removeTabImageButtonId = "remove_news_media_tab_image";
		assertTabImageDeletion(removeTabImageButtonId);
	}
	
	public void addMediaLink() {
		enterField(mediaLinkFieldId, mediaLinkUrl);
		enterField(mediaLinkTextFieldId, mediaLinkText);
		
		driver.findElement(By.id(mediaLinkTextFieldId)).sendKeys(Keys.ENTER);
		waitForDivToLoad();
		
		assertTextFieldChange(mediaLinkFieldId, mediaLinkUrl);
		assertTextFieldChange(mediaLinkTextFieldId, mediaLinkText);
		
		assertChangeInPreview(mediaLinkText);
	}
	
	public void writeMediaInformation() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("tinyMCE.activeEditor.setContent('<strong>"+mainMessage+"</strong>')");	
		
		String saveButtonXpath = "/html/body/div[5]/div[4]/div[1]/div[1]/div[1]/form/input[4]";
		WebElement saveButton = driver.findElement(By.xpath(saveButtonXpath));
		saveButton.click();
		waitForDivToLoad();
		
		assertChangeInPreview(mainMessage);
		
		assertTextIsEntered(mainMessage);		
		assertStyleIsAddedToText();		
	}
		
	private void assertStyleIsAddedToText() {
		String iframeId = "mce_editor_0_ifr";
		driver.switchTo().frame(iframeId);
		
		assertTrue(driver.findElement(By.tagName("strong")).isDisplayed());
	}
	
	
	
	
}
