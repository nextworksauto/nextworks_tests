package com.nextworks.selenium.junit_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.junit.Assert.assertTrue;

public class RolloverImages extends CMSActivity {
		
	String[] rolloverImages = {
		"/Users/ksafford/Desktop/Documents/test images/index.jpg",
		"/Users/ksafford/Desktop/Documents/test images/kitty.jpg",
		"/Users/ksafford/Desktop/Documents/test images/fishTwo.jpg"
	};
	
	public void addRolloverImageFromTheScratch(String addThumbnailButton, String addFileFromYourComputerRadioButton, String browseButton, String thumbnailFile, String browseRolloverImageButton, String rolloverImage) {
		WebElement thumbnailButton = driver.findElement(By.xpath(addThumbnailButton));
		thumbnailButton.click();
		
		WebElement addThumbnailFromComputer = driver.findElement(By.xpath(addFileFromYourComputerRadioButton));
		addThumbnailFromComputer.click();
		
		WebElement uploadThumbnail = driver.findElement(By.xpath(browseButton));
		uploadThumbnail.sendKeys(thumbnailFile);
		waitForDivToLoad();
		
		WebElement browseRolloverImage = driver.findElement(By.xpath(browseRolloverImageButton));
		browseRolloverImage.sendKeys(rolloverImage);
		waitForDivToLoad();
	}
	
	public void addRolloverImageToThumbnail(String addThumbnailButton, String browseRolloverImageButton, String rolloverImage) {
		WebElement thumbnailButton = driver.findElement(By.xpath(addThumbnailButton));
		thumbnailButton.click();
		
		WebElement browseRolloverImage = driver.findElement(By.xpath(browseRolloverImageButton));
		browseRolloverImage.sendKeys(rolloverImage);
		waitForDivToLoad();
	}
	
	public void assertRolloverImageIsAdded(String imageTitle) {
		WebElement uploadedImage = driver.findElement(By.cssSelector(imageTitle));
		assertTrue(!uploadedImage.getAttribute("data-rollover").isEmpty());
	}
}


