package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestCapsuleEmailOptions extends CapsuleEmailOptions{

	@BeforeClass
	public static void main() throws IOException {
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
	}
	
	@Test
	public void test() throws IOException{
		emailPanel();
		checkEmailPanel();	
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
