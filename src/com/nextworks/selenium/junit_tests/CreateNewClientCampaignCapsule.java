package com.nextworks.selenium.junit_tests;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;

public class CreateNewClientCampaignCapsule extends CMSActivity {
	
	String legalInfoMessage = "Legal Information Block";
	
	static Random rn = new Random();
	static String randomName = "KS_test_" + rn.nextInt(10000);
	
	static Date now = new Date();
	static String currentTime = "Test "+ DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(now);
	
	String capsule;
	
	public void createNewCapsule() throws FileNotFoundException, UnsupportedEncodingException {
		CMSActivity dr = new CMSActivity();
		dr.setWebDriver();
		CMSActivity url = new CMSActivity();
		driver.get(url.getContentURL());
		CMSActivity login = new CMSActivity();
		login.loginPage();
		createNewClientCampaignCapsule();
	}
		
	public void createNewClientCampaignCapsule() throws FileNotFoundException, UnsupportedEncodingException {
		//create New Client Account
		findByXpathAndClick("html/body/div[5]/div/div[1]/ul/li[5]/a");				
		findByIdAndSendkeys("client_accounts_company_name", currentTime);
		findByIdAndSendkeys("client_accounts_subdomain", randomName);
		findByIdAndSendkeys("client_accounts_contact", "Katya");
		findByIdAndSendkeys("client_accounts_contact_email", "ksafford@nextworks.com");
		findByNameAndClick("save");
				
		//create New Campaign 
		findByIdAndSendkeys("campaign_name", randomName);
		//findByIdAndClick("campaign_start_date");
		driver.findElement(By.cssSelector("input[name='campaign_start_date']")).click();
		//waitForElementPresent(".//*[@id='ui-datepicker-div']/div/a[2]/span");
		findByXpathAndClick(".//*[@id='ui-datepicker-div']/div/a[2]/span");
		findByXpathAndClick(".//*[@id='ui-datepicker-div']/table/tbody/tr[2]/td[1]/a");
		findByIdAndClick("campaign_end_date");
		findByXpathAndClick(".//*[@id='ui-datepicker-div']/div/a[2]/span");
		findByXpathAndClick(".//*[@id='ui-datepicker-div']/table/tbody/tr[2]/td[5]/a");
		findByNameAndClick("save");
				
		//create new capsule
		findByIdAndSendkeys("unit_name", currentTime);
		enterLegalInformation();
		findByNameAndClick("save");		
		waitForDivToLoad();
		
		String createdCapsule = driver.getCurrentUrl();
		if (createdCapsule.endsWith("#videos")) {
			createdCapsule = createdCapsule.substring(0, createdCapsule.length()-7);
		}
		capsule = createdCapsule;
		
		putCapsuleUrlInFile(capsule);
	}
	
	public void checkCreation() {
		setWebDriver();
		driver.get(getContentURL());
		driver.manage().window().maximize();		
		loginPage();
				
		List <WebElement> allCapsulesNames = driver.findElements(By.className("thumbnailtitle"));
		ArrayList<String> allCapsulesNamesInArray = new ArrayList<String>();
		for (int i=0; i<allCapsulesNames.size()-1; i++) {
			allCapsulesNamesInArray.add(allCapsulesNames.get(i).getText());
		}
				
		assertTrue(allCapsulesNamesInArray.contains(currentTime));
	}
		
	private void findByIdAndSendkeys(String fieldName, String value) {
		WebElement element = driver.findElement(By.id(fieldName));			
		element.sendKeys(value);
	}
	
	private void findByXpathAndClick(String xpath) {
		WebElement element = driver.findElement(By.xpath(xpath));			
		element.click();
	}
	
	private void findByNameAndClick(String name) {
		WebElement element = driver.findElement(By.name(name));			
		element.click();
	}
	
	private void findByIdAndClick(String id) {
		WebElement element = driver.findElement(By.id(id));			
		element.click();
	}
	
	private void enterLegalInformation() {
		driver.switchTo().frame(0);
		WebElement legalInfoBlock = driver.switchTo().activeElement();
		legalInfoBlock.sendKeys(legalInfoMessage);
		driver.switchTo().defaultContent();
	}

	private void putCapsuleUrlInFile(String capsuleLink) throws FileNotFoundException, UnsupportedEncodingException{
		File file = new File("NewCapsuleUrl.txt");
		if (file.exists()) {
			file.delete();
		}
		
		PrintWriter writer = new PrintWriter("NewCapsuleUrl.txt", "UTF-8");
		writer.println(capsuleLink);
		writer.close();
	}
}
