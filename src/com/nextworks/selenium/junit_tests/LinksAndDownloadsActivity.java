package com.nextworks.selenium.junit_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class LinksAndDownloadsActivity extends CMSActivity {

	public void openAndActivateLinkAndDownloadsTab() {
		String tabCheckBox = "show_in_capsule[links_downloads]";
		String clickOnTab = "li[data-tab_type='links_downloads']";
		openAndActivateTab(tabCheckBox, clickOnTab);
	}
	
	public void assertDragAndDrop(String xpath, String dispalyedName) {
		WebElement expectedLinkName = driver.findElement(By.xpath(xpath));
		String actualLinkName = expectedLinkName.getText();
		assertEquals(dispalyedName, actualLinkName);		
	}
	
	public void addLink(String linkUrl, String titleOfUrl) {
		WebElement addUrlCheckBox = driver.findElement(By.id("link"));
		if (!addUrlCheckBox.isSelected()) {
			addUrlCheckBox.click();
		}
		
		WebElement addUrlField = driver.findElement(By.xpath(".//*[@id='url'][@placeholder='Add full link url']"));
		addUrlField.clear();
		addUrlField.sendKeys(linkUrl);			
		WebElement urlTitle = driver.findElement(By.xpath(".//*[@id='title'][@placeholder='Add title']"));
		urlTitle.clear();
		urlTitle.sendKeys(titleOfUrl);
		urlTitle.sendKeys(Keys.ENTER);
		waitForDivToLoad();
	}
	
	public void linksDragAndDrop(String xpathOfAddedLink, String xpathOfEmbedLink) {
		WebElement linkToDrag = driver.findElement(By.xpath(xpathOfAddedLink));
		WebElement canvas = driver.findElement(By.id("links_downloads_grid"));		
		Actions builder = new Actions(driver);
		builder.dragAndDrop(linkToDrag, canvas).build().perform();
		waitForElementPresent(xpathOfEmbedLink);
	}
	
	public void addDownload(String pathToTheFile, String downloadFileName) {
		waitForElementPresent("/html/body/div[5]/div[4]/div[2]/div[15]/div[2]/div/input[2]");
		driver.findElement(By.xpath("/html/body/div[5]/div[4]/div[2]/div[15]/div[2]/div/input[2]")).click();
		driver.findElement(By.id("download_file")).sendKeys(pathToTheFile);
		waitForDivToLoad();
		WebElement downloadFilenameField = driver.findElement(By.xpath("/html/body/div[5]/div[4]/div[2]/div[15]/div[2]/form[3]/input[1]"));
		downloadFilenameField.clear();
		downloadFilenameField.sendKeys(downloadFileName);
		downloadFilenameField.sendKeys(Keys.ENTER);
		waitForDivToLoad();
	} 
		
	protected void deleteLinksAndDownloads(String xpath, String expandPanel) {
		WebElement deletionButton = driver.findElement(By.xpath(xpath));
		if (!deletionButton.isDisplayed()) {
			driver.findElement(By.cssSelector(expandPanel)).click();
		}
		deletionButton.click();
		waitForDivToLoad();		
	}
	
	public void assertTabLinkDeletion(String tabLinkName, String cssSelector) {
		List <WebElement> allTabLinkNames = driver.findElements(By.cssSelector(cssSelector));
		//System.out.println("The amount of assets in the system is "+allTabLinkNames.size()+". They are:");
		/*for (int i=0; i<=allTabLinkNames.size()-1; i++) {
			String allTabLinks = allTabLinkNames.get(i).getAttribute("data-title");
			System.out.println(allTabLinks);
		}*/
		
		ArrayList<String> allTabLinksInArray = new ArrayList<String>();
		for (int i=0; i<allTabLinkNames.size()-1; i++) {
			allTabLinksInArray.add(allTabLinkNames.get(i).getAttribute("data-title"));
		}
		
		assertFalse(allTabLinksInArray.contains(tabLinkName));
	}
	
}
