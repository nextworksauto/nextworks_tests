package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestCapsuleBrandingOptions extends CapsuleBrandingOptions{

	@BeforeClass		
	public static void main() throws IOException {
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
	}
 
	
	@Test
	public void test() throws IOException{	
		enterValues();
		checkInputs();			
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
