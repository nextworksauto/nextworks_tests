package com.nextworks.selenium.junit_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CapsuleBrandingOptions extends CMSActivity{
	
	static String capsule;
	
	private static final String BRANDING_LOGO = "White Logo";
	private static final String FONT_FAMILY = "Arial Black";
	private static final String FONT_COLOR = "#ff0000";
	private static final String CANVAS_COLOR = "#00ddff";
	private static final String CONTENT_BG_COLOR = "#eaff00";
	private static final String TAB_COLOR = "#000000";
	private static final String TAB_FONT_COLOR = "#ffffff";
	private static final String TOP_MARGIN = "60";
	private static final String HORIZONTAL_MARGIN = "15";
	//private static final String BORDER_COLOR = "#ff00dd";
	//private static final String HIGHLIGHT_COLOR = "#33ff00";
	//private static final String HIGHLIGHT_WIDTH = "5";
	//private static final String PERCENT_OPAQUE = "50";
	//private static final String OPACITY_COLOR = "#f52cc3";
	private static final String TAB_TITLE_FONT = "Georgia";
	private static final String TAB_TITLE_FONT_SIZE = "33";
	private static final String TAB_TITLE_FONT_COLOR = "#b3164d";
	private static final String CALL_TO_ACTION_BUTTON_NAME = "Click on me!";
	private static final String CALL_TO_ACTION_LINK = "www.google.com";
	private static final String CALL_TO_ACTION_FONT_COLOR = "#ffffff";
	private static final String CALL_TO_ACTION_BUTTON_COLOR = "#0009ff";
	String bgimage = "file://C:/test images/1024x768-purple.png";
	
	public static String getCapsule() {
		return capsule;
	}
 
	public void enterValues() {
		//open Capsule Branding Options
		driver.findElement(By.id("branding_panel_close")).click();
				
		driver.findElement(By.id("background_image")).sendKeys(bgimage);		
		dropdownList("next_logo", BRANDING_LOGO);
		driver.findElement(By.id("next_logo_left")).click();
		dropdownList("font", FONT_FAMILY);				
		enterField("font_colour", FONT_COLOR);
		enterField("canvas_colour", CANVAS_COLOR);				
		enterField("background_colour", CONTENT_BG_COLOR);
		enterField("tab_colour", TAB_COLOR);				
		enterField("tab_font_colour", TAB_FONT_COLOR);
				
		/*WebElement borderCheckBox = driver.findElement(By.id("use_border"));		
		if (borderCheckBox.isSelected()) {
			enterField("border_colour", BORDER_COLOR);
		}
		else {
		    borderCheckBox.click();
			enterField("border_colour", BORDER_COLOR);
		}*/
					
		//highlight(HIGHLIGHT_COLOR, HIGHLIGHT_WIDTH);
		//opacity(PERCENT_OPAQUE, OPACITY_COLOR);
				
		//margins
		enterField("top_margin", TOP_MARGIN);
		enterField("side_margin", HORIZONTAL_MARGIN);
		
		dropdownList("tab_content_title_font", TAB_TITLE_FONT);
		enterField("tab_content_title_size", TAB_TITLE_FONT_SIZE);
		enterField("tab_content_title_colour", TAB_TITLE_FONT_COLOR);
		enterField("call_to_action_text", CALL_TO_ACTION_BUTTON_NAME);
		enterField("call_to_action_url", CALL_TO_ACTION_LINK);
		enterField("call_to_action_color", CALL_TO_ACTION_FONT_COLOR);
		enterField("call_to_action_button_color", CALL_TO_ACTION_BUTTON_COLOR);
				
		driver.findElement(By.id("submit")).click();
			
		//close branding options section
		driver.findElement(By.id("branding_panel_close")).click();
			
	}
	
	public void checkInputs() throws IOException{
		//open capsule again and compare the values entered before with stored values
		//driver.get(getContentURL());
		//driver.findElement(By.cssSelector("a[href='"+capsule+"']")).click();
		//waitForDivToLoad();
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
		
		WebElement capsuleBrandingOptions = driver.findElement(By.id("branding_panel_close"));
		capsuleBrandingOptions.click();
		
		Select logo = new Select(driver.findElement(By.id("next_logo")));
		String selectedLogo = logo.getFirstSelectedOption().getText();
		assertEquals(selectedLogo,  BRANDING_LOGO);
				
		WebElement logoAlignment = driver.findElement(By.id("next_logo_left"));
		if (!logoAlignment.isSelected()) {
			System.out.println("ERROR. Something is wrong with logo alignment");
		}
				
		Select fontFamily = new Select(driver.findElement(By.id("font")));
		String selectedFontFamily = fontFamily.getFirstSelectedOption().getText();
		assertEquals(selectedFontFamily, FONT_FAMILY);
			
		String fontColorValue = driver.findElement(By.id("font_colour")).getAttribute("value");
		assertEquals(fontColorValue, FONT_COLOR);
				
		String canvasColorValue = driver.findElement(By.id("canvas_colour")).getAttribute("value");
		assertEquals(canvasColorValue, CANVAS_COLOR);
				
		String contentBackgroundColorValue = driver.findElement(By.id("background_colour")).getAttribute("value");
		assertEquals(contentBackgroundColorValue, CONTENT_BG_COLOR);
			
		String tabColorValue = driver.findElement(By.id("tab_colour")).getAttribute("value");
		assertEquals(tabColorValue, TAB_COLOR);
				
		String tabFontColorValue = driver.findElement(By.id("tab_font_colour")).getAttribute("value");
		assertEquals(tabFontColorValue, TAB_FONT_COLOR);
				
		//checkBorder(BORDER_COLOR);
		//checkHighlight(HIGHLIGHT_COLOR, HIGHLIGHT_WIDTH);
		//checkOpacity(PERCENT_OPAQUE, OPACITY_COLOR);
				
		WebElement mobileOptions = driver.findElement(By.id("mobile_on"));
		assertTrue(mobileOptions.isSelected());
						
		String topMarginValue = driver.findElement(By.id("top_margin")).getAttribute("value");
		assertEquals(topMarginValue, TOP_MARGIN);
		
		String horizontalMarginValue = driver.findElement(By.id("side_margin")).getAttribute("value");
		assertEquals(horizontalMarginValue, HORIZONTAL_MARGIN);
			
		Select tabFontFamily = new Select(driver.findElement(By.id("tab_content_title_font")));
		String selectedTabFontFamily = tabFontFamily.getFirstSelectedOption().getText();
		assertEquals(selectedTabFontFamily, TAB_TITLE_FONT);
						
		String tabTitleFontSizeValue = driver.findElement(By.id("tab_content_title_size")).getAttribute("value");
		assertEquals(tabTitleFontSizeValue, TAB_TITLE_FONT_SIZE);
						
		String tabTitleFontColorValue = driver.findElement(By.id("tab_content_title_colour")).getAttribute("value");
		assertEquals(tabTitleFontColorValue, TAB_TITLE_FONT_COLOR);
						
		String callToActionButtonValue = driver.findElement(By.id("call_to_action_text")).getAttribute("value");
		assertEquals(callToActionButtonValue, CALL_TO_ACTION_BUTTON_NAME);
						
		String callToActionLinkValue = driver.findElement(By.id("call_to_action_url")).getAttribute("value");
		assertEquals(callToActionLinkValue, CALL_TO_ACTION_LINK);
						
		String callToActionFontColorValue = driver.findElement(By.id("call_to_action_color")).getAttribute("value");
		assertEquals(callToActionFontColorValue, CALL_TO_ACTION_FONT_COLOR);
						
		String callToActionButtonColorValue = driver.findElement(By.id("call_to_action_button_color")).getAttribute("value");
		assertEquals(callToActionButtonColorValue, CALL_TO_ACTION_BUTTON_COLOR);
	}
	
	/*private void checkHighlight(String highlightColor, String highlightWidth) {
		WebElement hightlight = driver.findElement(By.id("use_highlight"));
		assertTrue(hightlight.isSelected());
		String expectedHighlightColor = highlightColor;
		String highlightColorValue = driver.findElement(By.id("highlight_colour")).getAttribute("value");
		assertEquals(expectedHighlightColor, highlightColorValue);
					
		String expectedHighlightWidth = highlightWidth;
		String highlightWidthValue = driver.findElement(By.id("highlight_width")).getAttribute("value");
		assertEquals(expectedHighlightWidth, highlightWidthValue);
	}
	
	private void checkOpacity(String percentOpaque, String opacityColor) {
		WebElement opacity = driver.findElement(By.id("use_opacity"));
		assertTrue(opacity.isSelected());
		
		WebElement opacityFadeStyle = driver.findElement(By.id("opacity_on"));
		assertTrue(opacityFadeStyle.isSelected());
		
		String expectedPercentOpaque = percentOpaque;
		String percentOpaqueValue = driver.findElement(By.id("opacity")).getAttribute("value");
		assertEquals(expectedPercentOpaque, percentOpaqueValue);
					
		String expectedOpacityColor = opacityColor;
		String opacityColorValue = driver.findElement(By.id("opacity_colour")).getAttribute("value");
		assertEquals(expectedOpacityColor, opacityColorValue);
	}
	
	private void checkBorder(String borderColor) {
		WebElement border = driver.findElement(By.id("use_border"));
		assertTrue(border.isSelected());
		String expectedBorderColor = borderColor;
		String borderColorValue = driver.findElement(By.id("border_colour")).getAttribute("value");
		assertEquals(expectedBorderColor, borderColorValue);
	}
	
	private void highlight(String highlightColor, String highlightWidth) {
		WebElement highlightCheckBox = driver.findElement(By.id("use_highlight"));
		if (highlightCheckBox.isSelected()) {				
			enterField("highlight_colour", highlightColor);			
			enterField("highlight_width", highlightWidth);
		}
		else {
			highlightCheckBox.click();			
			enterField("highlight_colour", highlightColor);			
			enterField("highlight_width", highlightWidth);
		}
	}
	
	private void opacity(String percentOpaque, String opacityColor) {
		WebElement opacityCheckBox = driver.findElement(By.id("use_opacity"));
		if (opacityCheckBox.isSelected()) {
			enterField("opacity", percentOpaque);
			enterField("opacity_colour", opacityColor);				
		}
		else {
			opacityCheckBox.click();
			enterField("opacity", percentOpaque);
			enterField("opacity_colour", opacityColor);
		}
	} 
	*/
	
	
	
		
}



























