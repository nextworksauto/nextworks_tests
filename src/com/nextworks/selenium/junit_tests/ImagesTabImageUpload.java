package com.nextworks.selenium.junit_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ImagesTabImageUpload extends ImagesTabActivity {
	
	String[] images = {
			"/Users/ksafford/Desktop/Documents/test images/3bears.jpg",
			"/Users/ksafford/Desktop/Documents/test images/peter_griffin_football_zoomed.png",
			"/Users/ksafford/Desktop/Documents/test images/catfish.jpg"
	};
	
	public String[] imagesNames = {
			"image1",
			"image2",
			"image3"
	};
	
	String[] imagesXpathes = {
			".//*[@id='image_list']/div[2]/img",
			".//*[@id='image_list']/div[3]/img",
			".//*[@id='image_list']/div[4]/img"
			//"/html/body/div[5]/div[4]/div[2]/div[11]/div[1]/div/div[2]/img",
			///"/html/body/div[5]/div[4]/div[2]/div[11]/div[1]/div/div[3]/img",
			//"/html/body/div[5]/div[4]/div[2]/div[11]/div[1]/div/div[4]/img"
	};
	
	String[] deleteXpathes = {
			"/html/body/div[5]/div[4]/div[2]/div[11]/div[1]/div/div[2]/a[2]/i",
			"/html/body/div[5]/div[4]/div[2]/div[11]/div[1]/div/div[3]/a[2]/i",
			"/html/body/div[5]/div[4]/div[2]/div[11]/div[1]/div/div[4]/a[2]/i"
	};
	
	String allFoundAssets = "img[data-content_item_type='image_upload']";
	
	String xpathToAssertDeletiionFromRightHandPanel = "img[class='ui-draggable thinglinkFiltered'][data-content_item_type='image_upload']";
	
	//String deleteFromCarousel = "/html/body/div[5]/div[4]/div[1]/div[1]/div[2]/div[3]/ul/li[1]/a";
	String deleteFromCarousel = "/html/body/div[5]/div[4]/div[1]/div[1]/div[1]/div[3]/ul/li[1]/a";

	public void uploadImage (String imageToUpload, String imageName)  {
		String addFileFromYourCpmputer = "/html/body/div[5]/div[4]/div[2]/div[11]/div[2]/div/input[2]";
		waitForElementPresent(addFileFromYourCpmputer);
		
		driver.findElement(By.id("file")).click();
		driver.findElement(By.id("image_upload_file")).sendKeys(imageToUpload);
		waitForDivToLoad();
								
		WebElement imageNameField = driver.findElement(By.id("image_upload_title"));
		imageNameField.sendKeys(imageName);
		imageNameField.sendKeys(Keys.ENTER);
		waitForDivToLoad();				
	}
	
	protected void dragAndDropUploadedImages (String xpath) {
		WebElement imageToDrag = driver.findElement(By.xpath(xpath));
		WebElement canvas = driver.findElement(By.id("image_preview"));
		
		Actions builder = new Actions(driver);
		builder.dragAndDrop(imageToDrag, canvas).build().perform();
		waitForDivToLoad();
		//driver.findElement(By.xpath("/html/body/div[5]/div[4]/div[1]/div[1]/div[2]/form/input[1]")).click();
		driver.findElement(By.id("embed_item")).click();
		waitForDivToLoad();
	}
	
	public String[] getImagesToUpload() {
		String[] images = {
				"file://C:/test images/3bears.jpg",
				"file://C:/test images/peter_griffin_football_zoomed.png",
				"file://C:/test images/catfish.jpg"
		};
		return images;
	}
	
		
	
}
