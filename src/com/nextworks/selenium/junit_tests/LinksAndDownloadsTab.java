package com.nextworks.selenium.junit_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class LinksAndDownloadsTab extends LinksAndDownloadsActivity{

	String[] urls = {
			"www.google.com",
			"www.translate.google.com",
			"www.maps.google.com"
	};
	
	String[] titles = {
			"google",
			"google translator",
			"google maps"
	};
	
	String[] filesToDownload = {
			"/Users/ksafford/Desktop/Documents/test images/3bears.jpg",
			"/Users/ksafford/Desktop/Documents/test images/catfish.jpg",
			"/Users/ksafford/Desktop/Documents/test images/ooyala1.txt"
	};
	
	String[] filesToDownloadNames ={
			"file1",
			"file2",
			"file3"
	};
	
	public void links() {
		openAndActivateLinkAndDownloadsTab();
				
		String[] urlsXpathes = {
				".//*[@id='item_list']/div[2]/img",
				".//*[@id='item_list']/div[3]/img",
				".//*[@id='item_list']/div[4]/img"
		};
		
		String[] urlsEmbeds = {
				".//*[@id='links_downloads_grid']/li",
				".//*[@id='links_downloads_grid']/li[2]",
				".//*[@id='links_downloads_grid']/li[3]"
		};
		
		for (int i=0; i<urls.length; i++) {
			addLink(urls[i], titles[i]);
		}
		
		for (int i=0; i<urls.length; i++) {
			assertAssetsCreation(urlsXpathes[i], titles[i]);
		}
		
		for (int i=0; i<urls.length; i++) {
			linksDragAndDrop(urlsXpathes[i], urlsEmbeds[i]);
		}
		
		String[] displayedNames = {
				titles[0]+" (link)",
				titles[1]+" (link)",
				titles[2]+" (link)"
		};
		
		String[] embedLinksXpathes = {
				".//*[@id='links_downloads_grid']/li[1]/p",
				".//*[@id='links_downloads_grid']/li[2]/p",
				".//*[@id='links_downloads_grid']/li[3]/p"
		};
		
		for (int i=0; i<urls.length; i++) {
			assertDragAndDrop(embedLinksXpathes[i], displayedNames[i]);
		}
			
	}
		
	public void downloads() {
		for (int i=0; i<urls.length; i++) {
			addDownload(filesToDownload[i], filesToDownloadNames[i]);
		}
		
		String[] downloadsXpathes = {
				".//*[@id='item_list']/div[5]/img",
				".//*[@id='item_list']/div[6]/img",
				".//*[@id='item_list']/div[7]/img"
		};
		
		String[] downloadsEmbedXpathes = {
				".//*[@id='links_downloads_grid']/li[4]",
				".//*[@id='links_downloads_grid']/li[5]",
				".//*[@id='links_downloads_grid']/li[6]"
		};
		
		for (int i=0; i<urls.length; i++) {
			assertAssetsCreation(downloadsXpathes[i], filesToDownloadNames[i]);
		}
		
		for (int i=0; i<urls.length; i++) {
			linksDragAndDrop(downloadsXpathes[i], downloadsEmbedXpathes[i]);
		}
		
		String[] displayedDownloadsNames = {
				filesToDownloadNames[0]+" (download)",
				filesToDownloadNames[1]+" (download)",
				filesToDownloadNames[2]+" (download)",
		};
		
		String[] embedDownloadsXpathes = {
				".//*[@id='links_downloads_grid']/li[4]/p",
				".//*[@id='links_downloads_grid']/li[5]/p",
				".//*[@id='links_downloads_grid']/li[6]/p"
		};
		
		for (int i=0; i<urls.length; i++) {
			assertDragAndDrop(embedDownloadsXpathes[i], displayedDownloadsNames[i]);
		}
	}
	
	public void deleteLinksAndDownloads() {
		String deleteButton = "/html/body/div[5]/div[4]/div[2]/div[15]/div[1]/div/div[2]/a[3]/i";
		String allLinks = "div[data-type='link']";
		String allDownloads = "div[data-type='download']";
		
		for (int i=0; i<urls.length; i++) {
			deleteAssets(deleteButton);
			assertTabLinkDeletion(titles[titles.length-i-1], allLinks);
		}
		
		for (int i=0; i<filesToDownload.length; i++) {
			deleteAssets(deleteButton);
			assertTabLinkDeletion(filesToDownloadNames[filesToDownloadNames.length-i-1], allDownloads);
		}
	}
		
	public String[] getLinks() {
		String[] urls = {
				"www.google.com",
				"www.translate.google.com",
				"www.maps.google.com"
		};
		return urls;
	}
	
	public String[] getFilesToDownload(){
			String[] filesToDownload = {
					"file://C:/test images/3bears.jpg",
					"file://C:/test images/catfish.jpg",
					"file://C:/test images/ooyala1.txt"
			};
			return filesToDownload;
	}
}
