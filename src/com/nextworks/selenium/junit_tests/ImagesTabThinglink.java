package com.nextworks.selenium.junit_tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ImagesTabThinglink extends ImagesTabActivity {
	
	String[] thinglinkImages = {
			"<img style=\"max-width:100%\" src=\"https://farm4.staticflickr.com/3909/14650906047_10f4c263fd_o.jpg#tl-553294976797114368;1043138249'\" class=\"alwaysThinglink\"/><script async charset=\"utf-8\" src=\"//cdn.thinglink.me/jse/embed.js\"></script>",
			"<img style=\"max-width:100%\" src=\"//cdn.thinglink.me/api/image/253858146089435138/1024/10/scaletowidth#tl-253858146089435138;1043138249'\" class=\"alwaysThinglink\"/><script async charset=\"utf-8\" src=\"//cdn.thinglink.me/jse/embed.js\"></script>",
			"<img style=\"max-width:100%\" src=\"http://www.dimmak.com/wp-content/uploads/2013/03/bootik-big.jpg#tl-371720261642223616;1043138249'\" class=\"alwaysThinglink\"/><script async charset=\"utf-8\" src=\"//cdn.thinglink.me/jse/embed.js\"></script>"
	};
	
	String[] thinglinkTitles = {
			"thinglink_image_1",
			"thinglink_image_2",
			"thinglink_image_3"
	};
	
	String[] thinglinkXpath = {
			".//*[@id='thing_list']/div[2]",
			".//*[@id='thing_list']/div[3]",
			".//*[@id='thing_list']/div[4]"
	};
	
	String allFoundAssets = "img[data-content_item_type='thinglink']";
	
	String deleteThinglink = "/html/body/div[5]/div[4]/div[2]/div[9]/div[1]/div[2]/a[2]/i";
	
	public void thinglinkUpload(String thingLinkImageURL, String thinglinkImageTitle)  {
		waitForDivToLoad();
		WebElement thinglinkUrl = driver.findElement(By.id("thinglink_embed"));
		thinglinkUrl.clear();
		thinglinkUrl.sendKeys(thingLinkImageURL);
		
		WebElement thinglinkTitle = driver.findElement(By.id("thinglink_title"));
		thinglinkTitle.sendKeys(thinglinkImageTitle);
		thinglinkTitle.sendKeys(Keys.ENTER);
		waitForDivToLoad();
	}
	
	/*public void dragAndDropImages (String xpath) {
		WebElement imageToDrag = driver.findElement(By.xpath(xpath));
		WebElement canvas = driver.findElement(By.id("image_preview"));
		
		Actions builder = new Actions(driver);
		builder.dragAndDrop(imageToDrag, canvas).build().perform();
		waitForDivToLoad();
		driver.findElement(By.id("embed_item")).click();
		waitForDivToLoad();
	}*/
}
