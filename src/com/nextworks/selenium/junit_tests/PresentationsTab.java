package com.nextworks.selenium.junit_tests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class PresentationsTab extends CMSActivity{

	String[] presentations = {
			"<iframe src=\"http://www.slideshare.net/slideshow/embed_code/44692272\" width=\"427\" height=\"356\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\" style=\"border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;\" allowfullscreen> </iframe> <div style=\"margin-bottom:5px\"> <strong> <a href=\"https://www.slideshare.net/MiTpacio/7-lessons-from-guy-kawasaki-they-wont-teach-you-at-business-school\" title=\"7 Lessons From Guy Kawasaki They Won&#x27;t Teach You At Business School \" target=\"_blank\">7 Lessons From Guy Kawasaki They Won&#x27;t Teach You At Business School </a> </strong> from <strong><a href=\"http://www.slideshare.net/MiTpacio\" target=\"_blank\">Andrea Pacini</a></strong> </div>",
			"<iframe src=\"http://www.slideshare.net/slideshow/embed_code/44747488\" width=\"427\" height=\"356\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\" style=\"border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;\" allowfullscreen> </iframe> <div style=\"margin-bottom:5px\"> <strong> <a href=\"https://www.slideshare.net/Bplans/30-reasons-to-start-a-business\" title=\"30 Reasons to Start a Business\" target=\"_blank\">30 Reasons to Start a Business</a> </strong> from <strong><a href=\"http://www.slideshare.net/Bplans\" target=\"_blank\">Bplans.com</a></strong> </div>",
			"<iframe src=\"http://www.slideshare.net/slideshow/embed_code/27757090\" width=\"427\" height=\"356\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\" style=\"border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;\" allowfullscreen> </iframe> <div style=\"margin-bottom:5px\"> <strong> <a href=\"https://www.slideshare.net/PhilipPowers1/morgan-logic\" title=\"A Quick Tour of Logos: The Logical Appeal\" target=\"_blank\">A Quick Tour of Logos: The Logical Appeal</a> </strong> from <strong><a href=\"http://www.slideshare.net/PhilipPowers1\" target=\"_blank\">Philip Powers</a></strong> </div>"
	};
	
	String[] presentationTitles = {
		"pres1",
		"pres2",
		"pres3"
	};
	
	String[] presentationsXpath ={
		"//*[@id='ss_list']/div[1]/img",
		"//*[@id='ss_list']/div[2]/img",
		"//*[@id='ss_list']/div[3]/img"
	};
	
	String presentationsTabCheckBox = "show_in_capsule[presentations]";
	
	String presentationsTab = "li[data-tab_type='presentations']";
	
	String cssSelectorForAllEmbedPresentations = "img[data-content_item_type='slideshare']";
	
	String deleteButton = ".//*[@id='ss_list']/div/a[2]/i";
	
	public void addPresentation(String presentationEmbedCode, String presentationTitle) {
		WebElement slideshareEmbedCodeArea = driver.findElement(By.id("slideshare_embed"));
		slideshareEmbedCodeArea.clear();
		slideshareEmbedCodeArea.sendKeys(presentationEmbedCode);
		WebElement slideshareTitle = driver.findElement(By.id("slideshare_title"));
		slideshareTitle.clear();
		slideshareTitle.sendKeys(presentationTitle);
		slideshareTitle.sendKeys(Keys.ENTER);
		waitForDivToLoad();		
	}
	
	public void assertPresentationCreation(String xpath, String givenTitle) {		
			WebElement presentation = driver.findElement(By.xpath(xpath));
			String presentationTitle = presentation.getAttribute("data-content_item_title");
			assertEquals(presentationTitle, givenTitle);
	} 
		
	public void presentationsDragAndDrop(String xpath, String videoPanel) {
			WebElement imageToDrag = driver.findElement(By.xpath(xpath));
			WebElement canvas = driver.findElement(By.id("presentations_preview"));
			if (!canvas.isDisplayed()) {
				driver.navigate().refresh();
			}
			Actions builder = new Actions(driver);
			builder.dragAndDrop(imageToDrag, canvas).build().perform();
			waitForDivToLoad();
			WebElement embedPresentationButton = driver.findElement(By.xpath("//input[@id='embed_content_item'][@value='Embed this presentation']"));
			embedPresentationButton.click();
			waitForDivToLoad();
			driver.navigate().refresh();
			waitForDivToLoad();
			driver.findElement(By.xpath(videoPanel)).click();
	}
	
	

	
}
