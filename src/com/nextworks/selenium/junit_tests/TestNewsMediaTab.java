package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestNewsMediaTab extends NewsMediaTab{
	
	@BeforeClass
	public static void main() throws IOException{
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
		
		String newsMediaCheckBox = "show_in_capsule[news_medias]";
		String newsMediaTab = "li[data-tab_type='news_medias']";
		
		CMSActivity tab = new CMSActivity();
		tab.openAndActivateTab(newsMediaCheckBox, newsMediaTab);
	}	
	
	@Test
	public void test() {
		changeNewsMediaTitle();
		changeTabImage();
		addMediaLink();
		writeMediaInformation();				
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
	
}
