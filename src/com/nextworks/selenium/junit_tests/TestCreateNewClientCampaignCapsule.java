package com.nextworks.selenium.junit_tests;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestCreateNewClientCampaignCapsule extends CreateNewClientCampaignCapsule{

	@BeforeClass
	public static void main() {
		CMSActivity dr = new CMSActivity();
		dr.setWebDriver();
		
		CMSActivity url = new CMSActivity();
		driver.get(url.getContentURL());
		
		CMSActivity login = new CMSActivity();
		login.loginPage();
	}
	
	@Test
	public void test() throws FileNotFoundException, UnsupportedEncodingException {
		createNewClientCampaignCapsule();
		driver.close();
		checkCreation();
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
