package com.nextworks.selenium.junit_tests;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestImagesTabFlickr extends ImagesTabFlickr{

	@BeforeClass
	public static void main() throws IOException{
		//String capsuleToOpen = "a[href='http://stg.contentcapsule.com/admin/unitcontent/capsules/edit/535']";
		
		//CMSActivity activity = new CMSActivity();
		//activity.navigateToCapsule(capsuleToOpen);
		CMSActivity activity = new CMSActivity();
		activity.navigateToCapsule(getCapsuleUrl());
	}
	
	@Test
	public void flickrSection() {
		openAndActivateImagesTab();
		expandFlickrSection();	
				
		for (int i=0; i<flickrUrls.length; i++) {
			flickrUpload(flickrUrls[i], flickrTitles[i]);
		}
		assertAccetsCreationByCount(allFoundAssets, flickrUrls);
		
		for (int i=0; i<flickrUrls.length; i++) {
			dragAndDropImages(flickrXpath[i]);
		}		
		
		for (int i=0; i<flickrUrls.length; i++) {
			assertDragAndDropDividedByTwo(allFoundAssets, flickrUrls);
		}
		
		//delete from right-hand panel		
		String deleteButton = "/html/body/div[5]/div[4]/div[2]/div[6]/div[1]/div/div[2]/a[2]/i";
		for (int i=0; i<flickrUrls.length; i++) {
			deleteAssets(deleteButton);
		}
		
		for (int i=0; i<flickrUrls.length; i++) {
			assertImagesDeletion(flickrTitles[i], allFoundAssets);
		}
		assertDeletionByAmount(allFoundAssets);		
	}
	
	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}
}
