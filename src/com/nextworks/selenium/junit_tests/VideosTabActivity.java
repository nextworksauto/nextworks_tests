package com.nextworks.selenium.junit_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


public class VideosTabActivity extends CMSActivity{

	public void openAndActivateVideosTab() {
		String checkBoxOnTab = "show_in_capsule[videos]";
		String tabCssSelector = "li[data-tab_type='videos']";
		openAndActivateTab(checkBoxOnTab, tabCssSelector);
	}
	
	public void videosDragAndDrop(String xpath, String videoPanel) {
		WebElement imageToDrag = driver.findElement(By.xpath(xpath));
		WebElement canvas = driver.findElement(By.id("videos_preview"));
		if (!canvas.isDisplayed()) {
			driver.navigate().refresh();
		}
		Actions builder = new Actions(driver);
		builder.dragAndDrop(imageToDrag, canvas).build().perform();
		waitForDivToLoad();
		driver.findElement(By.id("embed_item")).click();
		waitForDivToLoad();
		driver.navigate().refresh();
		waitForDivToLoad();
		driver.findElement(By.xpath(videoPanel)).click();
	}
	
	
	
}
