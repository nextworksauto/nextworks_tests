package com.nextworks.selenium.junit_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ImagesTabActivity extends CMSActivity {

	public void expandImageUploadSection() {
		WebElement expandImageUploadSection = driver.findElement(By.xpath("/html/body/div[5]/div[4]/div[2]/div[10]/a"));
		expandImageUploadSection.click();
	}
	
	public void expandThinglinkSection() {
		WebElement expandThinglinkSection = driver.findElement(By.cssSelector("a[data-widget='thinglink']"));
		expandThinglinkSection.click();
	}
	
	public void expandFlickrSection() {
		String expandFlickrPanel = "a[data-widget='flickr']";
		driver.findElement(By.cssSelector(expandFlickrPanel)).click();
	}
	
	protected void dragAndDropImages (String xpath) {
		WebElement imageToDrag = driver.findElement(By.xpath(xpath));
		WebElement canvas = driver.findElement(By.id("image_preview"));
		
		Actions builder = new Actions(driver);
		builder.dragAndDrop(imageToDrag, canvas).build().perform();
		waitForDivToLoad();
		driver.findElement(By.id("embed_item")).click();
		waitForDivToLoad();
	}
		
	public void openAndActivateImagesTab() {
		String checkBoxOnTab = "show_in_capsule[images]";
		String clickOnTab = "li[data-tab_type='images']";
		openAndActivateTab(checkBoxOnTab, clickOnTab);
	}
			
	public void assertImagesDeletion(String imageName, String xpath) {
		List <WebElement> allImagesNames = driver.findElements(By.xpath(xpath));
		//System.out.println("The total amount of images in the system is "+allImagesNames.size());
		ArrayList<String> allTabLinksInArray = new ArrayList<String>();
		for (int i=0; i<allImagesNames.size()-1; i++) {
			allTabLinksInArray.add(allImagesNames.get(i).getAttribute("title"));
		}
		assertFalse(allTabLinksInArray.contains(imageName));
	}
	
	public void assertAccetsCreationByCount(String cssSelector, String[] arrayOfAssets) {
		List <WebElement> allAssets = driver.findElements(By.cssSelector(cssSelector));
		assertEquals(allAssets.size(), arrayOfAssets.length);
	}
	
	
}
